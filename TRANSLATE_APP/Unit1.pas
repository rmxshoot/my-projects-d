
unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,System.UITypes, Vcl.Graphics,
  Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Tabs, Vcl.Themes,
  Vcl.ComCtrls, Vcl.ExtDlgs, Vcl.Menus, IdBaseComponent, IdComponent, Vcl.OleCtrls,
  SHDocVw, Character, inifiles,ShellApi, Vcl.Controls;

type
 TComboBox = Class(Vcl.StdCtrls.TComboBox)
    function DoMouseWheel(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint): Boolean; override;
  private
    FUseMouseWheel: Boolean;
  public
    Property UseMouseWheel: Boolean Read FUseMouseWheel Write FUseMouseWheel;
  End;

type
  TForm1 = class(TForm)
    Button1: TButton;
    ScrollBox1: TScrollBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    ScrollBox2: TScrollBox;
    Panel1: TPanel;
    ComboBox1: TComboBox;
    OpenTextFileDialog1: TOpenTextFileDialog;
    Button2: TButton;
    SaveTextFileDialog1: TSaveTextFileDialog;
    Button3: TButton;
    SaveTextFileDialog2: TSaveTextFileDialog;
    Button4: TButton;
    OpenTextFileDialog2: TOpenTextFileDialog;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    About1: TMenuItem;
    Loadsourcefile1: TMenuItem;
    SaveEnglishstrings1: TMenuItem;
    Savetranslations1: TMenuItem;
    Exit1: TMenuItem;
    TabSheet3: TTabSheet;
    WebBrowser1: TWebBrowser;
    Button5: TButton;
    Panel2: TPanel;
    ProgressBar1: TProgressBar;
    Changelog1: TMenuItem;
    Help1: TMenuItem;
    Button6: TButton;
    procedure Button1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    function ProcessString(I:Integer; lang:String):String;
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    function AddLang(index:Integer;ComboBox_Text,Lang_Name:String):Integer;
    procedure Button5Click(Sender: TObject);
    procedure AddLang_Form_OK(Sender: TObject);
    procedure Edit1Click(Sender: TObject);
    procedure Edit2Click(Sender: TObject);
    procedure onResize(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Edit2KeyPress(Sender: TObject;var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LinkOnClick(Sender : TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheel2(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure ScrollBox1Click(Sender: TObject);
    procedure Changelog1Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure EditEnter(Sender: TObject);

    private

    public

  End;

type 
  TLang = Record
    ComboBox_Text:string;
    Lang_Name:String  
  end;

const
  VERSION = 'v1.3';

var
  Form1: TForm1;
  local_no_p1:Integer;
  local_no_p2:Integer;
  general_no:Integer;
  general_left_val:Integer;
  arLineLabel:array [0..1300] of TLabel;
  arTrans_EDIT:array [0..1300] of TEdit;
  scrollSellecter:TScrollBox;
  lineNum:TLabel;
  vert_line:TLabel;
  StringList,StringList2:TStringList;
  after_first_line:Boolean;
  loaded_already:Boolean;
  Source_loaded:Boolean;
  languages : array [0..20] of TLang;
  index:Integer;
  AddLang_Form:TForm;
  AddLang_Label1:TLabel;
  AddLang_Edit1:TEdit;
  AddLang_Label2:TLabel;
  AddLang_Edit2:TEdit;
  AddLang_Link:TLabel;
  AddLang_Button:TButton;
  IniFile : TIniFile;
  lang_text:String;
  lang_code:String;
  Source_num_lines,Translation_num_lines:Integer;

  trans_val:array [1..1300] of String;
  source_val:array [1..1300] of String;
  

implementation

{$R *.dfm}

procedure TForm1.About1Click(Sender: TObject);
var
AboutForm: TForm;
begin
  AboutForm:=CreateMessageDialog('Translating Application by BeGiN'+#10#13+
                                'Version: '+VERSION +#10#13+#10#13+
                                '� 2014 BeGiN@Soft',mtInformation,[mbOK],mbOK);
  try
  begin
    AboutForm.Caption:='Translate application by BeGiN';
    AboutForm.ShowModal;
  end;
  finally
    AboutForm.Free;
  end;
end;

procedure TForm1.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
Var
  msg: Cardinal;
  code: Cardinal;
  i, n: Integer;
begin
    Handled := true;
    If ssShift In Shift Then
      msg := WM_HSCROLL
    Else
      msg := WM_VSCROLL;

    If WheelDelta > 0 Then
      code := SB_LINEUP
    Else
      code := SB_LINEDOWN;

    n:= Mouse.WheelScrollLines;
    For i:= 1 to n Do
      scrollbox1.Perform( msg, code, 0 );
    scrollbox1.Perform( msg, SB_ENDSCROLL, 0 );
end;

procedure TForm1.FormMouseWheel2(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
Var
  msg: Cardinal;
  code: Cardinal;
  i, n: Integer;
begin
    Handled := true;
    If ssShift In Shift Then
      msg := WM_HSCROLL
    Else
      msg := WM_VSCROLL;

    If WheelDelta > 0 Then
      code := SB_LINEUP
    Else
      code := SB_LINEDOWN;

    n:= Mouse.WheelScrollLines;
    For i:= 1 to n Do
      scrollbox2.Perform( msg, code, 0 );
    scrollbox2.Perform( msg, SB_ENDSCROLL, 0 );
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
IniFile.Free;
end;

function TComboBox.DoMouseWheel(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint): Boolean;
begin
 if FUseMouseWheel then inherited
 else Result := true;
end;

procedure Tform1.FormCreate(Sender: TObject);
begin
  Form1.Caption:='Translating Application '+VERSION;
  loaded_already:=false;
  if FileExists(ChangeFileExt(Application.ExeName,'.ini')) then begin
    IniFile:= TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
    lang_text:=IniFile.ReadString('Langs','lang_text','');
    ComboBox1.Items.Add(lang_text);
    ComboBox1.Update;
    lang_code := IniFile.ReadString('Langs','lang_code','');
  end;
  
end;

procedure TForm1.EditEnter(Sender: TObject);
var
  Edit: TEdit absolute Sender;
begin
  Edit.SelStart := Length(Edit.Text);
end;



//LOAD SOURCE FILE
procedure TForm1.Button1Click(Sender: TObject);
var
  text   : string;
  SmyFile : TStringList;
  nofile : Boolean;
  Encoding: TEncoding;
  I:Integer;
begin
  if not Source_loaded then begin
    
    OpenTextFileDialog1:= TOpenTextFileDialog.Create(Self);
    OpenTextFileDialog1.InitialDir := ExtractFilePath(Application.ExeName);
    OpenTextFileDialog1.Filter := 'INI File|*.ini|Text File|*.txt';
    if OpenTextFileDialog1.Execute then begin
      SmyFile := TStringList.Create();
      SmyFile.LoadFromFile(OpenTextFileDialog1.FileName,TEncoding.UTF8);
      nofile:=false;
    end else nofile:=true;
    
    Form1.Caption:=Form1.Caption +' - '+ ExtractFileName(OpenTextFileDialog1.FileName);
  
    OpenTextFileDialog1.Free;
    general_no:=0;
    general_left_val:=33;
    
    if not nofile then begin
    Panel2.show;
    for I := 0 to SmyFile.Count-1 do
      begin
        text:= SmyFile.Strings[I];
        if (Pos('en.',text) = 1) or (Pos('en.',text) = 3)  then
        begin
          //FIRST PAGE
          if general_no <= 600 then
          begin
            local_no_p1:=general_no;
            scrollSellecter:=ScrollBox1;
            vert_line := TLabel.Create(scrollSellecter);
            with vert_line do
            begin
              Parent := scrollSellecter;
              Left:= 28;
              Top := 30+local_no_p1*45;
              Caption:= '|'
            end;
            lineNum := TLabel.Create(scrollSellecter);
            with lineNum do
            begin
              Parent := scrollSellecter;
              Left := 2;
              Top := 30+local_no_p1*45;
              Caption:= IntToStr(general_no+1);
            end;
            arLineLabel[general_no] := TLabel.Create(scrollSellecter);
            with arLineLabel[general_no] do
            begin
              Parent := scrollSellecter;
              Left := general_left_val;
              Top := 30+local_no_p1*45;
              Caption:=text;
              Name:='Label'+IntToStr(local_no_p1);
            end;
            arTrans_EDIT[general_no]:= TEdit.Create(scrollSellecter);
            with arTrans_EDIT[general_no] do
            begin
              Visible:=false;
              Parent := scrollSellecter;
              Left := general_left_val;
              Top := 15+ arLineLabel[local_no_p1].Top;
              OnEnter:=EditEnter;
              Width :=arLineLabel[local_no_p1].Width+100;
              Text:=' ';
  //            if local_no_p1=0 then
  //              Text:='[CustomMessages]';
              Name:='Edit'+IntToStr(local_no_p1);
              Visible:=true;
              OnMouseWheel:=OnMouseWheel;
            end;
          end
          //SECOND PAGE
          else if (general_no > 600) and (general_no <= 1300) then begin 
            PageControl1.Pages[1].TabVisible:=true;
            PageControl1.Pages[0].Caption:='Page 1';
            local_no_p2:=general_no - 600;
            scrollSellecter:=ScrollBox2;
            vert_line := TLabel.Create(scrollSellecter);
            with vert_line do
            begin
              Parent := scrollSellecter;
              Left:= 28;
              Top := -20+local_no_p2*45;
              Caption:= '|'
            end;
            lineNum := TLabel.Create(scrollSellecter);
            with lineNum do
            begin
              Parent := scrollSellecter;
              Left := 2;
              Top := -20+local_no_p2*45;
              Caption:= IntToStr(general_no+1);
            end;
            arLineLabel[general_no] := TLabel.Create(scrollSellecter);
            with arLineLabel[general_no] do
            begin
              Parent := scrollSellecter;
              Left := general_left_val;
              Top := -20+local_no_p2*45;
              Caption:=text;
              Name:='Label'+IntToStr(local_no_p2);
            end;
            arTrans_EDIT[general_no] := TEdit.Create(scrollSellecter);
            with arTrans_EDIT[general_no] do
            begin
              Visible := false;
              Parent := scrollSellecter;
              Left := general_left_val;
              Top := 15+ arLineLabel[general_no].Top;
              Width :=arLineLabel[local_no_p2].Width+100;
              text:=' ';
              Name:='Edit'+IntToStr(local_no_p2);
              Visible:=true;
            end;
          end;
          general_no:=general_no+1;
          ProgressBar1.Parent:=Panel2;
          ProgressBar1.Min:=0;
          ProgressBar1.Max:=900;
          ProgressBar1.StepBy(15);
          ProgressBar1.Position:=general_no;

          if general_no>0 then begin
            ComboBox1.Enabled:=true;
            Button5.Enabled:=true;
            if arTrans_EDIT[0] <> nil then
              arTrans_EDIT[0].SetFocus;
          end;
        end;
      end;
      Panel2.Hide;
      Button3.Enabled:=true;
      Savetranslations1.Enabled:=true;
      Source_loaded:=true;
      Source_num_lines:=SmyFile.Count;
    end;
  end else
    ShowMessage('Source file loaded already! :('+#10#13+ 'You need to restart the application to load another Source.');
end;

function TForm1.ProcessString(I:Integer;lang:String):String;
var
  Source,LANG_temp,PIC_temp,LINK_temp,Target:String;
begin
  Source:=arLineLabel[I].Caption;
  LANG_temp:=Source;
  PIC_temp:=Source;
  LINK_temp:=Source;
  //delete language code
  Delete(LANG_temp,1,3);
  //insert new language code
  Insert(lang,LANG_temp,0);
  //deletes everything after =
  Delete(LANG_temp,Pos('=',LANG_temp)+1,550);
  Target:=LANG_temp;
  Result:=Target;

  if Pos('.jpg',PIC_temp) > 0 then begin
    Delete(PIC_temp,0,Pos('=',PIC_temp));
    Insert(PIC_temp,LANG_temp,Pos('=',PIC_temp)+1);
    //delete language code
    Delete(PIC_temp,1,3);
    //insert new language code
    Insert(lang,PIC_temp,0);
    Result:=PIC_temp;
  end;

  if Pos('http://',LINK_temp) > 0 then begin
    Delete(LINK_temp,0,Pos('=',LINK_temp));
    Insert(LINK_temp,LANG_temp,Pos('=',LINK_temp)+1);
    //delete language code
    Delete(LINK_temp,1,3);
    //insert new language code
    Insert(lang,LINK_temp,0);
    Result:=LINK_temp;
  end;

end;


procedure TForm1.ScrollBox1Click(Sender: TObject);
begin
if arTrans_EDIT[0] <> nil then
    arTrans_EDIT[0].SetFocus;
end;


procedure TForm1.TabSheet1Show(Sender: TObject);
begin
  Panel1.Show;
  if arTrans_EDIT[0] <> nil then
    arTrans_EDIT[0].SetFocus;
end;

procedure TForm1.TabSheet2Show(Sender: TObject);
begin
  Panel1.Show;
  if arTrans_EDIT[601] <> nil then
    arTrans_EDIT[601].SetFocus;
end;

procedure TForm1.TabSheet3Show(Sender: TObject);
begin
  Panel1.Hide;
  if not loaded_already then begin
    WebBrowser1.Navigate('https://translate.google.com/');
    loaded_already:=true;
  end;
end;

//SAVE ENGLISH STRINGS
procedure TForm1.Button3Click(Sender: TObject);
var
I:Integer;
Encoding:TEncoding;
begin
  StringList2:= TStringlist.Create;
  for I := 0 to 1300 do StringList2.Add(arLineLabel[I].Caption);
  SaveTextFileDialog2:= TSaveTextFileDialog.Create(Self);
  SaveTextFileDialog2.InitialDir := ExtractFilePath(Application.ExeName);
  SaveTextFileDialog2.Title := 'Save your text or INI file';
  SaveTextFileDialog2.Filter :='INI File|*.ini|Text File|*.txt';
  SaveTextFileDialog2.DefaultExt := 'ini';
  if SaveTextFileDialog2.Execute then begin
    Encoding := StandardEncodingFromName(SaveTextFileDialog2.Encodings[SaveTextFileDialog2.EncodingIndex]);
    StringList2.SaveToFile(SaveTextFileDialog2.FileName,Encoding);
    SaveTextFileDialog1.FileName:='English_Sourcefile'
  end else;
  SaveTextFileDialog2.Free;

end;

function ExtractText(const Str: string; const Delim1, Delim2: char): string;
var
  pos1, pos2: integer;
begin
  result := '';
  pos1 := Pos(Delim1, Str);
  pos2 := Pos(Delim2, Str);
  if (pos1 > 0) and (pos2 > pos1) then
    result := Copy(Str, pos1 + 1, pos2 - pos1 - 1);
end;

//LOAD TRANSLATION
procedure TForm1.Button4Click(Sender: TObject);
var
  trans_line   : string;
  smyFile2:TStringlist;
  nofile : Boolean;
  I , J : Integer;
begin
  OpenTextFileDialog2:= TOpenTextFileDialog.Create(Self);
  OpenTextFileDialog2.InitialDir := ExtractFilePath(Application.ExeName);
  OpenTextFileDialog2.Filter := 'INI File|*.ini|Text File|*.txt';
  if OpenTextFileDialog2.Execute then begin
    smyFile2:=TStringList.Create();
    smyFile2.LoadFromFile(OpenTextFileDialog2.FileName,TEncoding.UTF8);
    Translation_num_lines:=smyFile2.Count;
    nofile:=false;
  end else nofile:=true;
  
  OpenTextFileDialog2.Free;
  try
    if not nofile then begin
      J := 0;
      I := 0;
      while I <= Translation_num_lines do begin
        trans_line:= SmyFile2.Strings[I];
        Delete(trans_line,1,Pos('=',trans_line));
        trans_val[I]:=ExtractText(SmyFile2.Strings[I+1],'.','=');
        source_val[J]:=ExtractText(arLineLabel[J].Caption,'.','=');
        if (arTrans_EDIT[J] <> nil) and (Pos('.jpg',trans_line)=0) and (Pos('http://',trans_line)=0) then begin
          if source_val[J] = trans_val[I] then begin
              arTrans_EDIT[J].Text:= arTrans_EDIT[J].Text + trans_line;
              I:=I+1;
              J:=J+1;
          end else begin
            J:=J+1;
          end;
        end else begin
          if source_val[J] = trans_val[I] then begin
              arTrans_EDIT[J].Text:= arTrans_EDIT[J].Text;
              I:=I+1;
              J:=J+1;
          end else begin
            J:=J+1;
          end;      
        end;
      end;
    end;
    Except
      if not nofile then
        ShowMessage('Translation file incompatible! Source line has '+IntToStr(Source_num_lines)+ ' lines, while the translation has '+ IntToStr(Translation_num_lines)+' lines');

    

  end;
  
end;



function TForm1.AddLang(index:Integer;ComboBox_Text,Lang_Name:String):Integer;
var 
I:Integer;
begin
  languages[index].ComboBox_Text:=ComboBox_Text;
  languages[index].Lang_Name:=Lang_Name;

  if ComboBox1.Text = ComboBox_Text then begin
    for I := 0 to general_no do
      arTrans_EDIT[I].Text:= ProcessString(I,Lang_Name);
  end;
  index:=index+1;
  Result:=index;
end;

procedure TForm1.AddLang_Form_OK(Sender: TObject);
begin
  if AddLang_Edit1.Text <> '' then begin
    IniFile := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
    IniFile.WriteString('Langs','lang_text','_'+AddLang_Edit1.Text);
    IniFile.WriteString('Langs','lang_code',AddLang_Edit2.Text+'.');
    ComboBox1.Items.Append('_'+AddLang_Edit1.Text);
    ComboBox1.Update;
    AddLang_Form.Close;
  end else
    AddLang_Form.Close; 
end;

procedure TForm1.Edit1Click(Sender: TObject);
begin
  AddLang_Edit1.SetFocus;
end;

procedure TForm1.Edit2Click(Sender: TObject);
begin
  AddLang_Edit2.SetFocus;
end;

procedure TForm1.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
  if Key = '.' then
    Key := #0;
end;

procedure TForm1.Edit2Change(Sender: TObject);
begin
  if (AddLang_Edit2.Text= '') then begin
    AddLang_Edit2.Text:='unknown';
    AddLang_Edit2.SelectAll;
  end;
end;

procedure TForm1.onResize(Sender: TObject);
begin
  AddLang_Button.Left :=(AddLang_Form.Width - AddLang_Button.Width) div 2;  
end;

procedure TForm1.LinkOnClick(Sender : TObject);
begin
  ShellExecute(0, 'OPEN', 'http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes', '', '', SW_SHOWNORMAL);
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  AddLang_Form:= TForm.Create(Form1);
  AddLang_Form.Parent:=Form1;
  AddLang_Form.Caption:='Add your language!';
  AddLang_Form.Height:=230;
  AddLang_Form.Width:=300;
  AddLang_Form.Position:=poDesigned;
  AddLang_Form.Left:=Form1.Width div 2 - 170;
  AddLang_Form.Top:=Form1.Height div 2 - 170;
  AddLang_Form.Show;
  AddLang_Form.BorderStyle:=bsDialog;
  AddLang_Form.OnResize:=onResize;

  AddLang_Label1:=TLabel.Create(AddLang_Form);
  AddLang_Label1.Caption:='String to show in Drop-down list. (e.g: to English)';
  AddLang_Label1.Parent:=AddLang_Form;
  AddLang_Label1.Font.Size:=10;
  AddLang_Label1.Left:=5;
  AddLang_Label1.Top:=10;
  AddLang_Edit1:=TEdit.Create(AddLang_Form);
  AddLang_Edit1.Parent:=AddLang_Form;
  AddLang_Edit1.Top:=AddLang_Label1.Top+28;
  AddLang_Edit1.Left:=10;
  AddLang_Edit1.Width:=260;
  AddLang_Edit1.OnClick:=Edit1Click;
  
  AddLang_Label2:=TLabel.Create(AddLang_Form);
  AddLang_Label2.Caption:='Language code. (e.g: en)';
  AddLang_Label2.Parent:=AddLang_Form;
  AddLang_Label2.Font.Size:=10;
  AddLang_Label2.Left:=5;
  AddLang_Label2.Top:=AddLang_Edit1.Top+35;
  AddLang_Link:=TLabel.Create(AddLang_Form);
  AddLang_Link.Parent:=AddLang_Form;
  AddLang_Link.Font.Size:=10;
  AddLang_Link.Top:=AddLang_Label2.Top;
  AddLang_Link.Left:=AddLang_Label2.Left+AddLang_Label2.Width+8;
  AddLang_Link.Caption:='ISO 639-1 lang-codes';
  AddLang_Link.OnClick:=LinkOnClick;
  AddLang_Link.Font.Style:=AddLang_Link.Font.Style+[fsUnderline];
  AddLang_Link.Font.Color:=clHighlight;
  AddLang_Link.Cursor:=crHandPoint;
  AddLang_Edit2:=TEdit.Create(AddLang_Form);
  AddLang_Edit2.Parent:=AddLang_Form;
  AddLang_Edit2.Text:='unknown';
  AddLang_Edit2.Top:=AddLang_Label2.Top+28;
  AddLang_Edit2.Left:=10;
  AddLang_Edit2.Width:=260;
  AddLang_Edit2.OnClick:=Edit2Click;
  AddLang_Edit2.OnChange:=Edit2Change;
  AddLang_Edit2.OnKeyPress:=Edit2KeyPress;
  
  AddLang_Button:=TButton.Create(AddLang_Form);
  AddLang_Button.Parent:=AddLang_Form;
  AddLang_Button.Top:=AddLang_Edit2.Top+ 50;
  AddLang_Button.OnClick:=AddLang_Form_OK;
  AddLang_Button.Left :=(AddLang_Form.Width - AddLang_Button.Width) div 2;
  AddLang_Button.Caption:= 'OK'  
  
end;


procedure TForm1.Button6Click(Sender: TObject);
var I:Integer;
begin
  for I := 0 to general_no-1 do begin
   if (arTrans_EDIT[I].Text[pos('=',arTrans_EDIT[I].Text)+1]='') then begin
    arTrans_EDIT[I].SetFocus;
    Break;
   end;
  end;
end;

//SAVE TRANSLATION
procedure TForm1.Button2Click(Sender: TObject);
var
  I:Integer;
  Encoding : TEncoding;
begin
  StringList:= TStringList.Create;
  for I := 0 to general_no-1 do StringList.Add(arTrans_EDIT[I].Text);
  SaveTextFileDialog1:= TSaveTextFileDialog.Create(Self);
  SaveTextFileDialog1.InitialDir := ExtractFilePath(Application.ExeName);
  SaveTextFileDialog1.Title := 'Save your text or XML file';
  SaveTextFileDialog1.Filter :='Text File|*.txt|INI File|*.ini';
  SaveTextFileDialog1.DefaultExt := 'ini';

  //'to French':
  if ComboBox1.Text = 'to French' then SaveTextFileDialog1.FileName:='fr_translation';
  //'to German':
  if ComboBox1.Text = 'to German' then SaveTextFileDialog1.FileName:='de_translation';
  //'to Greek':
  if ComboBox1.Text = 'to Greek' then SaveTextFileDialog1.FileName:='gr_translation';
  //'to Italian':
  if ComboBox1.Text = 'to Italian' then SaveTextFileDialog1.FileName:='it_translation';
  //'to Polish':
  if ComboBox1.Text = 'to Polish' then SaveTextFileDialog1.FileName:='pl_translation';
  //'to Portuguese':
  if ComboBox1.Text = 'to Portuguese' then SaveTextFileDialog1.FileName:='pt_translation';
  //'to Spanish':
  if ComboBox1.Text = 'to Spanish' then SaveTextFileDialog1.FileName:='es_translation';

  if SaveTextFileDialog1.Execute then begin
    Encoding := StandardEncodingFromName(SaveTextFileDialog1.Encodings[SaveTextFileDialog1.EncodingIndex]);
    StringList.SaveToFile(SaveTextFileDialog1.FileName,Encoding);
  end else;
  SaveTextFileDialog1.Free;
end;

procedure TForm1.Changelog1Click(Sender: TObject);
var
Changelog: TForm;
label_changelog, text_box, ver_num: Tlabel;
begin
  Changelog:=TForm.Create(Form1);
  try
  begin
    Changelog.Position:=poOwnerFormCenter;
    Changelog.Caption:='What''s New?';
    Changelog.Height:=165;
    Changelog.Width:=300;
    Changelog.BorderStyle:=bsDialog;
    
    label_changelog:= TLabel.Create(Changelog);
    label_changelog.Parent:=Changelog;
    label_changelog.Caption:='Changelog';
    label_changelog.Font.Style:=[fsUnderline];
    label_changelog.Font.Size:=12;
    label_changelog.Top:=5;
    label_changelog.Width:=Changelog.Width-15;
    label_changelog.Alignment:=taCenter;

    ver_num:=TLabel.Create(Changelog);
    ver_num.Parent:=Changelog;
    ver_num.Top:=20;
    ver_num.Width:=Changelog.Width-15;
    ver_num.Left:=25;
    ver_num.Caption:= VERSION;
    ver_num.Font.Size:=12;
    ver_num.Font.Style:=[fsBold];
    ver_num.Alignment:=taCenter;
    
    text_box:=TLabel.Create(Changelog);
    text_box.Parent:=Changelog;
    text_box.Top:=30;
    text_box.Left:=20;
    text_box.Width:=Changelog.Width;
    text_box.Caption:=' '+#10+
                      '� Fixed some serious bugs regarding the "Reading access '+#10+
                      '   error"'+#10+
                      '� Showing "Incompatibility" Error if the Source file has' +#10+
                      '   less lines than the translation'+#10+
                      '� Code improvements and small bugfixes '+#10;
    Changelog.ShowModal;
  end;
  finally
    Changelog.Free;
  end;
end;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
  if not (ComboBox1.Text = 'Translating language') then
  begin
    Button4.Enabled:=true;
    Button2.Enabled:=true;
    SaveEnglishstrings1.Enabled:=true;
  end;
  //'to French':
  AddLang(index,'to French','fr.');
  //'to German':
  AddLang(index,'to German','de.');
  //'to Greek':
  AddLang(index,'to Greek','gr.');
  //'to Italian':
  AddLang(index,'to Italian','it.');
  //'to Polish':
  AddLang(index,'to Polish','pl.');
  //'to Portuguese':
  AddLang(index,'to Portuguese','pt.');
  //'to Spanish':
  AddLang(index,'to Spanish','es.');
  //when adding a new language
  AddLang(index,'_'+AddLang_Edit1.Text, AddLang_Edit2.Text+'.');
  //old added language from .ini
  AddLang(index,lang_text, lang_code);

  if WebBrowser1.HandleAllocated then begin
    //'to French':
    if ComboBox1.Text = 'to French' then WebBrowser1.Navigate('https://translate.google.com/#en/fr');
    //'to German':
    if ComboBox1.Text = 'to German' then WebBrowser1.Navigate('https://translate.google.com/#en/de');
    //'to Greek':
    if ComboBox1.Text = 'to Greek' then WebBrowser1.Navigate('https://translate.google.com/#en/el');
    //'to Italian':
    if ComboBox1.Text = 'to Italian' then WebBrowser1.Navigate('https://translate.google.com/#en/it');
    //'to Polish':
    if ComboBox1.Text = 'to Polish' then WebBrowser1.Navigate('https://translate.google.com/#en/pl');
    //'to Portuguese':
    if ComboBox1.Text = 'to Portuguese' then WebBrowser1.Navigate('https://translate.google.com/#en/pt');
    //'to Spanish':
    if ComboBox1.Text = 'to Spanish' then WebBrowser1.Navigate('https://translate.google.com/#en/es');
    //'to undefined':
    if ComboBox1.Text = lang_text then WebBrowser1.Navigate('https://translate.google.com/#en/en');

    
  end;
  
end;


end.
