object Form1: TForm1
  Left = 0
  Top = 0
  ClientHeight = 447
  ClientWidth = 801
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnActivate = FormCreate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 801
    Height = 392
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Page'
      OnShow = TabSheet1Show
      object ScrollBox1: TScrollBox
        Tag = -1
        Left = 0
        Top = 0
        Width = 793
        Height = 364
        VertScrollBar.Tracking = True
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        OnClick = ScrollBox1Click
        OnMouseWheel = FormMouseWheel
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Page 2'
      ImageIndex = 1
      TabVisible = False
      OnShow = TabSheet2Show
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ScrollBox2: TScrollBox
        Left = 0
        Top = 0
        Width = 793
        Height = 364
        VertScrollBar.Tracking = True
        Align = alClient
        TabOrder = 0
        OnMouseWheel = FormMouseWheel2
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Google Translate'
      ImageIndex = 2
      OnShow = TabSheet3Show
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object WebBrowser1: TWebBrowser
        Left = 0
        Top = 0
        Width = 793
        Height = 364
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 163
        ExplicitTop = 120
        ExplicitWidth = 300
        ExplicitHeight = 150
        ControlData = {
          4C000000F55100009F2500000100000001020000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 406
    Width = 801
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      801
      41)
    object Button1: TButton
      Left = 268
      Top = 6
      Width = 125
      Height = 25
      Hint = 'Press here to load the .txt with the lines to translate'
      Anchors = [akRight, akBottom]
      Caption = 'LOAD SOURCE FILE'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = Button1Click
    end
    object ComboBox1: TComboBox
      Left = 6
      Top = 8
      Width = 125
      Height = 21
      Anchors = [akLeft, akBottom]
      Enabled = False
      Sorted = True
      TabOrder = 1
      Text = 'Translating language'
      OnChange = ComboBox1Change
      Items.Strings = (
        'to French'
        'to German'
        'to Greek'
        'to Italian'
        'to Polish'
        'to Portuguese'
        'to Spanish')
    end
    object Button2: TButton
      Left = 667
      Top = 6
      Width = 125
      Height = 25
      Hint = 'Press here to save the translation for future usage'
      Anchors = [akRight, akBottom]
      Caption = 'SAVE TRANSLATION'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 536
      Top = 6
      Width = 125
      Height = 25
      Hint = 'Press here to save the lines starting with en'
      Anchors = [akRight, akBottom]
      Caption = 'SAVE ENGLISH STRINGS'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 399
      Top = 6
      Width = 125
      Height = 26
      Hint = 'Press here to load a previous translation'
      Anchors = [akRight, akBottom]
      Caption = 'LOAD TRANSLATION'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 137
      Top = 6
      Width = 88
      Height = 25
      Hint = 'Add a new language.'
      Anchors = [akLeft, akBottom]
      Caption = 'ADD LANGUAGE'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 231
      Top = 6
      Width = 31
      Height = 25
      Hint = 'Go to first unstranslated line'
      Caption = 'V'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnClick = Button6Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 392
    Width = 801
    Height = 14
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object ProgressBar1: TProgressBar
      Left = 0
      Top = 0
      Width = 801
      Height = 14
      Align = alClient
      TabOrder = 0
    end
  end
  object OpenTextFileDialog1: TOpenTextFileDialog
    Left = 728
    Top = 24
  end
  object SaveTextFileDialog1: TSaveTextFileDialog
    Left = 728
    Top = 120
  end
  object SaveTextFileDialog2: TSaveTextFileDialog
    Left = 728
    Top = 168
  end
  object OpenTextFileDialog2: TOpenTextFileDialog
    Left = 728
    Top = 72
  end
  object MainMenu1: TMainMenu
    Left = 728
    Top = 224
    object File1: TMenuItem
      Caption = 'File'
      object Loadsourcefile1: TMenuItem
        Caption = 'Load source file'
        OnClick = Button1Click
      end
      object SaveEnglishstrings1: TMenuItem
        Caption = 'Save English strings'
        Enabled = False
        OnClick = Button3Click
      end
      object Savetranslations1: TMenuItem
        Caption = 'Save translation'
        Enabled = False
        OnClick = Button2Click
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
      end
    end
    object Help1: TMenuItem
      Caption = 'Help'
      object Changelog1: TMenuItem
        Caption = 'What'#39's new?'
        OnClick = Changelog1Click
      end
      object About1: TMenuItem
        Caption = 'About Translating App...'
        OnClick = About1Click
      end
    end
  end
end
