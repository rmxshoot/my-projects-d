object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Split Application v.1.0'
  ClientHeight = 220
  ClientWidth = 482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    482
    220)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 9
    Top = 31
    Width = 193
    Height = 50
    TabOrder = 0
    object Label1: TLabel
      Left = 143
      Top = 17
      Width = 3
      Height = 13
      Alignment = taCenter
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 113
      Height = 33
      Caption = 'Load Translation'
      TabOrder = 0
      TabStop = False
      OnClick = Button1Click
    end
  end
  object Panel2: TPanel
    Left = 8
    Top = 95
    Width = 193
    Height = 49
    TabOrder = 1
    object Label2: TLabel
      Left = 143
      Top = 16
      Width = 3
      Height = 13
      Alignment = taCenter
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Button2: TButton
      Left = 8
      Top = 8
      Width = 113
      Height = 33
      Caption = 'Split Translation'
      Enabled = False
      TabOrder = 0
      TabStop = False
      OnClick = Button2Click
    end
  end
  object Panel3: TPanel
    Left = 280
    Top = 36
    Width = 194
    Height = 107
    Anchors = [akTop, akRight]
    BevelOuter = bvNone
    TabOrder = 2
    object Label4: TLabel
      Left = 9
      Top = 13
      Width = 174
      Height = 26
      Caption = 'Add or remove identifier for the line ( Ex. en )'
      WordWrap = True
    end
    object Button3: TButton
      Left = 15
      Top = 72
      Width = 66
      Height = 25
      Caption = 'Insert'
      TabOrder = 0
      TabStop = False
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 103
      Top = 74
      Width = 58
      Height = 25
      Caption = 'Remove'
      TabOrder = 1
      TabStop = False
      OnClick = Button4Click
    end
    object Edit1: TEdit
      Left = 8
      Top = 45
      Width = 73
      Height = 21
      TabStop = False
      TabOrder = 2
      OnKeyUp = Edit1KeyUp
    end
  end
  object Panel4: TPanel
    Left = 208
    Top = 11
    Width = 66
    Height = 147
    Anchors = [akTop]
    BevelOuter = bvNone
    TabOrder = 3
    DesignSize = (
      66
      147)
    object Label3: TLabel
      Left = 10
      Top = 8
      Width = 34
      Height = 13
      Alignment = taCenter
      Caption = 'ListBox'
    end
    object ListBox1: TListBox
      AlignWithMargins = True
      Left = 8
      Top = 23
      Width = 49
      Height = 114
      TabStop = False
      Anchors = [akTop]
      ItemHeight = 13
      TabOrder = 0
      OnClick = ListBox1Click
    end
  end
  object Panel5: TPanel
    Left = 8
    Top = 164
    Width = 466
    Height = 48
    BevelOuter = bvNone
    TabOrder = 4
    DesignSize = (
      466
      48)
    object Label5: TLabel
      Left = 1
      Top = 5
      Width = 3
      Height = 13
    end
    object ProgressBar1: TProgressBar
      Left = 0
      Top = 23
      Width = 465
      Height = 25
      Anchors = [akBottom]
      TabOrder = 0
    end
  end
  object OpenTextFileDialog1: TOpenTextFileDialog
    Left = 8
    Top = 376
  end
end
