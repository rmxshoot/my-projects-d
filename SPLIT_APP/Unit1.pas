unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtDlgs, Vcl.ExtCtrls,
  Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    OpenTextFileDialog1: TOpenTextFileDialog;
    Button2: TButton;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    ListBox1: TListBox;
    Edit1: TEdit;
    Button3: TButton;
    Button4: TButton;
    Label3: TLabel;
    Label4: TLabel;
    ProgressBar1: TProgressBar;
    Label5: TLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure Edit1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  line : string;
  SmyFile : TStringList;
  isFile: Boolean;
  l_List: array [1..10] of TStringList;
  LastPeekMessageTime: Cardinal = 0;

implementation

{$R *.dfm}


function ItemExists(ListBox: TListBox; const Item: string): Boolean;
begin
  Result := ListBox.Items.IndexOf(Item) >= 0;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  I:Integer;
  temp_line:string;
  extra_temp:string;
begin
  OpenTextFileDialog1:= TOpenTextFileDialog.Create(Self);
  OpenTextFileDialog1.InitialDir := ExtractFilePath(Application.ExeName);
  OpenTextFileDialog1.Filter := 'Text File|*.txt|INI File|*.ini';
  isFile:=false;
  if OpenTextFileDialog1.Execute then begin
    SmyFile := TStringList.Create();
    SmyFile.LoadFromFile(OpenTextFileDialog1.FileName,TEncoding.UTF8);
    Form1.Caption:=Form1.Caption +' - '+ ExtractFileName(OpenTextFileDialog1.FileName);
    isFile:=true;
    Label1.Caption:='OK';
    Label1.Font.Color:=clGreen;
    Button2.Enabled:=true;

    for I := 0 to 20 do begin
      temp_line:=SmyFile.Strings[I];
      if Pos ('.' , temp_line) = 3 then begin
        extra_temp:=copy(temp_line,1,2);
        if not ItemExists(ListBox1,extra_temp) then
          ListBox1.Items.Add(extra_temp);
        ListBox1.Update;
      end;
    end;
  end;
  OpenTextFileDialog1.Free;
end;

procedure TellWindowsWeArentFrozen;
var
  Msg: TMsg;
begin
  if GetTickCount <> LastPeekMessageTime then
  begin
    PeekMessage(Msg, 0, 0, 0, PM_NOREMOVE);
    LastPeekMessageTime := GetTickCount;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  I:integer;
  J:integer;
begin
  if isFile then begin
    if ListBox1.Count > 0 then begin
      ProgressBar1.Min:=0;
      ProgressBar1.Max:=SmyFile.Count-1;
      for J := 0 to ListBox1.Count-1 do l_List[J] := TStringList.Create;
      for I := 0 to SmyFile.Count-1 do begin
        TellWindowsWeArentFrozen;
        line:=SmyFile.Strings[I];
        ProgressBar1.Position:=I;
        Label5.Caption:=line;
        Label5.Update;
        for J := 0 to ListBox1.Count-1 do begin
          if Pos(ListBox1.Items.Strings[J],line) = 1 then begin
            l_List[J].Append(line);
            l_List[J].SaveToFile(ExtractFilePath(Application.ExeName)+'\'+ListBox1.Items.Strings[J]+'_translation.ini',TEncoding.UTF8);
          end;
        end;
        if ProgressBar1.Position = ProgressBar1.Max then begin
          Label5.Caption:='Done.';
          Label2.Caption:='OK';
          Label2.Font.Color:=clGreen;
        end;
      end;
    end else ShowMessage('ListBox is empty :(');
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  edit_string:String;
  is_there:boolean;
  I:Integer;
begin
  if not (Edit1.Text = '') then begin
    is_there:=false;
    for I := 0 to ListBox1.Count-1 do begin
     if Edit1.Text = ListBox1.Items[I] then is_there:=true;
    end;
    if not is_there then begin
      ListBox1.Items.Add(Edit1.Text);
    end;
    Edit1.Text:='';
  end;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  if not (Edit1.Text = '') then begin
    if not (ListBox1.Items.IndexOf(Edit1.Text) = -1) then
      ListBox1.Items.Delete(ListBox1.Items.IndexOf(Edit1.Text))
    else
      ShowMessage('Error - Item: "'+trim(Edit1.Text)+'" not found!');
    Edit1.Text:='';
  end;
end;

procedure TForm1.Edit1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key= VK_TAB then begin
    Button3.Click;
  end;
end;

procedure TForm1.ListBox1Click(Sender: TObject);
begin
  if ListBox1.ItemIndex = -1 then
    Exit;
  Edit1.Text:=ListBox1.Items[ListBox1.ItemIndex];
end;

end.
