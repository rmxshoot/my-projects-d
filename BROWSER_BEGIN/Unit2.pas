unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.OleCtrls, SHDocVw,
  Vcl.ExtCtrls;

type
  TForm2 = class(TForm)
    HomeButton: TButton;
    NextButton: TButton;
    BackButton: TButton;
    SearchButton: TButton;
    SearchField: TEdit;
    ReloadButton: TButton;
    WebBrowser1: TWebBrowser;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Timer1: TTimer;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    procedure BackButtonClick(Sender: TObject);
    procedure HomeButtonClick(Sender: TObject);
    procedure ReloadButtonClick(Sender: TObject);
    procedure NextButtonClick(Sender: TObject);
    procedure SearchButtonClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}


procedure TForm2.BackButtonClick(Sender: TObject);
begin
   WebBrowser1.GoBack;
end;

procedure TForm2.ReloadButtonClick(Sender: TObject);
begin
  WebBrowser1.Refresh;
end;

procedure TForm2.NextButtonClick(Sender: TObject);
begin
  WebBrowser1.GoForward;
end;

procedure TForm2.SearchButtonClick(Sender: TObject);
begin
  WebBrowser1.Navigate(SearchField.Text);
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
if not SearchField.Focused then
  SearchField.Text:=WebBrowser1.LocationURL;
if WebBrowser1.LocationName='' then begin
  Form2.Caption:='Browser by BeGiN';
end
else
begin
  Form2.Caption:='Browser by BeGiN - '+WebBrowser1.LocationName;
end;
Label1.Caption:=WebBrowser1.LocationURL;
end;

procedure TForm2.HomeButtonClick(Sender: TObject);
begin
  SearchField.Text:='http://www.google.com';
  WebBrowser1.Navigate('http://www.google.com');
end;

end.
