object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 542
  ClientWidth = 950
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 950
    Height = 65
    Align = alTop
    TabOrder = 1
    object Panel3: TPanel
      Left = 862
      Top = 1
      Width = 87
      Height = 63
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object SearchButton: TButton
        Left = 6
        Top = 7
        Width = 75
        Height = 25
        Align = alCustom
        Caption = 'Search'
        Default = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SearchButtonClick
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 136
      Height = 63
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object BackButton: TButton
        Left = 8
        Top = 7
        Width = 25
        Height = 25
        Hint = 'Back'
        Caption = '<-'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BackButtonClick
      end
      object HomeButton: TButton
        Left = 103
        Top = 7
        Width = 26
        Height = 25
        Hint = 'Home'
        Caption = 'H'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = HomeButtonClick
      end
      object NextButton: TButton
        Left = 39
        Top = 7
        Width = 26
        Height = 25
        Hint = 'Next'
        Caption = '->'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = NextButtonClick
      end
      object ReloadButton: TButton
        Left = 71
        Top = 7
        Width = 26
        Height = 25
        Hint = 'Reload'
        Caption = 'R'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = ReloadButtonClick
      end
    end
    object Panel5: TPanel
      Left = 137
      Top = 1
      Width = 725
      Height = 63
      Align = alClient
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 2
      object SearchField: TEdit
        AlignWithMargins = True
        Left = 3
        Top = 8
        Width = 719
        Height = 25
        Margins.Top = 8
        Margins.Bottom = 30
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        ExplicitHeight = 26
      end
    end
  end
  object WebBrowser1: TWebBrowser
    Left = 0
    Top = 65
    Width = 950
    Height = 463
    Align = alClient
    TabOrder = 0
    ExplicitLeft = -1
    ExplicitTop = 67
    ControlData = {
      4C0000002F620000DA2F00000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126208000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object Panel2: TPanel
    Left = 0
    Top = 528
    Width = 950
    Height = 14
    Align = alBottom
    TabOrder = 2
    object Label1: TLabel
      Left = 0
      Top = 0
      Width = 3
      Height = 13
    end
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = Timer1Timer
    Top = 64
  end
end
