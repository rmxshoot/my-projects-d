unit Unit4;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.ExtCtrls, Registry, ShellAPI,
  TlHelp32, Vcl.Themes;

type
  TForm4 = class(TForm)
    TopPanel: TPanel;
    BotPanel: TPanel;
    Logger: TRichEdit;
    exe_load: TStaticText;
    inf_load: TStaticText;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    ExeButton: TButton;
    InfButton: TButton;
    BATButton: TButton;
    inst_running: TStaticText;
    Timer1: TTimer;
    ThemeSellector: TComboBox;
    Theme_stxt: TStaticText;
    procedure infLoaderClick(Sender: TObject);
    procedure exeLoaderClick(Sender: TObject);
    procedure InitializeForm(Sender: TObject);
    procedure BATButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OnTimer(Sender: TObject);
    procedure ThemeChanger(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;
  openDialog : TOpenDialog;    // Open dialog variable
  infParam,exeParam : String;
  temp_created: Boolean;
  fdefaultStyleName:String;
  WorkingDir:String;

implementation

{$R *.dfm}

function LoadFileToStr(const FileName: TFileName): AnsiString;
var
  FileStream : TFileStream;
begin
  FileStream:= TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
     if FileStream.Size>0 then
     begin
      SetLength(Result, FileStream.Size);
      FileStream.Read(Pointer(Result)^, FileStream.Size);
     end;
    finally
     FileStream.Free;
    end;
end;

(*
  Return the current location of one of the standard folders.
  Examples include 'Temp', 'Start Menu', 'Programs', 'Startup',
  'Desktop', 'Personal', 'Favorites', 'Fonts', 'SendTo', 'Recent',
  'NetHood', 'PrintHood', 'Templates', 'AppData'.
*)
function StandardFolder(const Which: string): string;
const
  RegFolders =
    '\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders';
var R: TRegistry; Buffer: array[0..1023] of Char;
begin
  Result := ''; (* Default return value *)
  if AnsiCompareText(Which, 'TEMP') = 0 then
    (* Get TEMP directory *)
    SetString(Result, Buffer, GetTempPath(Sizeof(Buffer) - 1, Buffer))
  else begin
    (* Get User folder *)
    R := TRegistry.Create;
    try
      if R.OpenKey(RegFolders, False) then
        try
          Result := R.ReadString(Which)
        except (* Result = '' *)
        end
    finally
      R.Free
    end
  end
end;

function processExists(exeFileName: string): Boolean; 
var 
  ContinueLoop: BOOL; 
  FSnapshotHandle: THandle; 
  FProcessEntry32: TProcessEntry32; 
begin 
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0); 
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32); 
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32); 
  Result := False; 
  while Integer(ContinueLoop) <> 0 do 
  begin 
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) = 
      UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) = 
      UpperCase(ExeFileName))) then 
    begin 
      Result := True; 
    end; 
    ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32); 
  end; 
  CloseHandle(FSnapshotHandle); 
end; 


procedure Execute(FileName, Parameter: string);
begin
ShellExecute(Application.Handle,
nil,
PChar(FileName), // Filename with path
PChar(Parameter), // Parameters (not needed)
PChar(Parameter), // path of file
SW_HIDE);
end;

procedure TForm4.infLoaderClick(Sender: TObject);
begin
  openDialog := TOpenDialog.Create(self);
  try
    openDialog.InitialDir := WorkingDir;
    openDialog.Options := [ofFileMustExist];
    openDialog.Filter :='_Aslains_Installer_Options.inf | *.inf';
    openDialog.FilterIndex := 1;
    if openDialog.Execute then
    begin
      inf_load.Font.Color:=clGreen;
      inf_load.Caption:='LOADED';
      Logger.Text:=Logger.Text + 'File: '+ openDialog.FileName+ '  - Successfully loaded!' + #10;
      infParam:=openDialog.FileName;
      BATButton.Enabled:=true;
      Logger.Text:=Logger.Text + 'Created: _Aslains_Installer_Options.inf' + #10#13;
      CopyFile(PChar(infparam),PChar(WorkingDir + 'Loadinf_TEMP\_Aslains_Installer_Options.inf'),false );
    end
  finally
    openDialog.Free;
  end;
end;

procedure TForm4.exeLoaderClick(Sender: TObject);
begin
  openDialog := TOpenDialog.Create(self);
  try
    openDialog.InitialDir := WorkingDir;
    openDialog.Options := [ofFileMustExist];
    openDialog.Filter := 'Aslain''s Installer Executable | *.exe';
    openDialog.FilterIndex := 1;
    if openDialog.Execute then
    begin
      exe_load.Font.Color:=clGreen;
      exe_load.Caption:='LOADED';
      Logger.Text:=Logger.Text + 'File: '+ ExtractFileName(openDialog.FileName)+ '  - Successfully loaded!' + #10;
      InfButton.Enabled:=true;
      exeParam:=openDialog.FileName;
      Logger.Text:=Logger.Text + 'Created: aslains_installer.exe'+ #10#13;
      CopyFile(PChar(exeparam),PChar(WorkingDir+'Loadinf_TEMP\aslains_installer.exe'),false );
    end
  finally
    openDialog.Free;
  end;
end;

procedure TForm4.ThemeChanger(Sender: TObject);
begin
   //windows, Glossy, Auric, Light, Emeral Light Slate
   if Assigned(TStyleManager.ActiveStyle) then begin
   
     if ThemeSellector.Text='Default' then begin
      if TStyleManager.TrySetStyle(fdefaultStyleName) then begin
        Logger.Text:=Logger.Text+ ThemeSellector.Text +' Theme successfully applied' +#10#13; 
        Logger.Font.Color:=clDefault;
        exe_load.Color:=clBtnFace;
      end else
        Logger.Text:=Logger.Text+ 'Error loading '+ ThemeSellector.Text +' Theme ' +#10#13; 
     end;
     
     if ThemeSellector.Text='Glossy' then begin 
      if TStyleManager.TrySetStyle('Glossy') then begin
        Logger.Text:=Logger.Text+ ThemeSellector.Text +' Theme successfully applied' +#10#13;
        Logger.Font.Color:=cl3DLight;
        exe_load.Transparent:=false;
        exe_load.Color:=clred;
      end else
        Logger.Text:=Logger.Text+ 'Error loading '+ ThemeSellector.Text +' Theme ' +#10#13; 
     end;    
     
     if ThemeSellector.Text='Auric' then begin 
      if TStyleManager.TrySetStyle('Auric') then begin
        Logger.Text:=Logger.Text+ ThemeSellector.Text +' Theme successfully applied' +#10#13;
        Logger.Font.Color:=cl3DLight;
      end else
        Logger.Text:=Logger.Text+ 'Error loading '+ ThemeSellector.Text +' Theme ' +#10#13;
     end;
     
     if ThemeSellector.Text='Light' then begin 
      if TStyleManager.TrySetStyle('Light')then begin
        Logger.Text:=Logger.Text+ ThemeSellector.Text +' Theme successfully applied' +#10#13;
        Logger.Font.Color:=clDefault;
      end else
        Logger.Text:=Logger.Text+ 'Error loading '+ ThemeSellector.Text +' Theme ' +#10#13;
     end;
     
     if ThemeSellector.Text='Emerald Light Slate' then begin 
      if TStyleManager.TrySetStyle('Emerald Light Slate') then begin
        Logger.Text:=Logger.Text+ ThemeSellector.Text +' Theme successfully applied' +#10#13;
        Logger.Font.Color:=clDefault;
      end else
        Logger.Text:=Logger.Text+ 'Error loading '+ ThemeSellector.Text +' Theme ' +#10#13;
     end;
     
   end;
end;

procedure TForm4.OnTimer(Sender: TObject);
begin
  if processExists('aslains_installer.exe') then begin
    inst_running.Caption:='RUNNING';
    inst_running.Font.Color:=clGreen;
    inst_running.Refresh;
  end 
  else 
  begin
    inst_running.Caption:='NOT RUNNING';
    inst_running.Font.Color:=clRed;
    inst_running.Refresh;
  end;
end;

procedure TForm4.BATButtonClick(Sender: TObject);
begin
  if (exe_load.Caption='LOADED') or (exe_load.Caption='AUTO-LOADED') then
  begin
    Execute(WorkingDir+'Loadinf_TEMP\loadinf.bat', 'aslains_installer.exe');
    Logger.Text:=Logger.Text + 'Executed: aslains_installer.exe /LOADINF=_Aslains_Installer_Options.inf'+#10;
  end
  else
    ShowMessage('Installer not LOADED!');
end;

function GetSystemDrive: string;
begin
SetLength(Result, MAX_PATH);
if GetWindowsDirectory(PChar(Result), MAX_PATH) > 0 then
begin
SetLength(Result, StrLen(PChar(Result)));
Result := ExtractFileDrive(Result);
end else
RaiseLastOSError;
end;

procedure TForm4.InitializeForm(Sender: TObject);
var 
  batch: TStringList;
  searchResult : TSearchRec;
begin
  if Assigned(TStyleManager.ActiveStyle) then
    fdefaultStyleName := TStyleManager.ActiveStyle.Name;
  
  Form4.Caption:= 'LOADINF for Aslain''s Installer v2.3 by BeGiN';
  Logger.Text:= 'LOADINF for Aslain''s Installer v2.3  ' + #10;
  Logger.Text:=Logger.Text + 'Author: BeGiN' + #10#13;
  temp_created:=false;

  WorkingDir:= ExtractFilePath(Application.ExeName);
  Logger.Text:=Logger.Text + 'Working Directory: "'+ WorkingDir +' "' + #10#13; 
  
  if ForceDirectories(WorkingDir+'Loadinf_TEMP') then begin
    FileSetAttr(WorkingDir+'Loadinf_TEMP',faHidden);
    Logger.Text:=Logger.Text + 'Created: Temporary folder'+ #10#13;
    temp_created:=true;
    batch:=TStringList.Create;
    batch.Add('cd %~dp0');
    batch.Add('%1 /LOADINF=_Aslains_Installer_Options.inf'); 
    batch.SaveToFile(WorkingDir+'Loadinf_TEMP\loadinf.bat');
    if FileExists(WorkingDir+'Loadinf_TEMP\loadinf.bat') then begin
      Logger.Text:=Logger.Text + 'Created: loadinf.bat'+ #10#13;
    end
    else
      Logger.Text:=Logger.Text + 'Created: loadinf.bat - ERROR'+#10;

  
    //Check for Aslains_XVM_Mod_Installer_*.exe
    if FindFirst('Aslains_XVM_Mod_Installer_*', faAnyFile, searchResult) = 0 then begin
      exe_load.Font.Color:=clGreen;
      exe_load.Caption:='AUTO-LOADED';
      Logger.Text:=Logger.Text + 'File: "'+ searchResult.Name + '" - Successfully auto-loaded!' + #10;
      InfButton.Enabled:=true;
      exeParam:=WorkingDir+searchResult.Name;
      if not FileExists(WorkingDir+'Loadinf_TEMP\aslains_installer.exe') then begin
        Logger.Text:=Logger.Text + 'Created: aslains_installer.exe'+ #10#13;
        CopyFile(PChar(exeparam),PChar(WorkingDir+'Loadinf_TEMP\aslains_installer.exe'),false );
      end;
      FindClose(searchResult);
    end
    else
      Logger.Text:=Logger.Text +'File: Aslains_XVM_Mod_Installer_*.exe  - Not found!' + #10#13;

      
    //Check for _Aslains_Installer_Options.inf
    if FileExists(WorkingDir+'_Aslains_Installer_Options.inf') then begin
      inf_load.Font.Color:=clGreen;
      inf_load.Caption:='AUTO-LOADED';
      Logger.Text:=Logger.Text + 'File: " _Aslains_Installer_Options.inf " - Successfully auto-loaded!' + #10;
      infParam:=WorkingDir+'_Aslains_Installer_Options.inf';
      InfButton.Enabled:=true;
      BATButton.Enabled:=true;
      if not FileExists(WorkingDir+'Loadinf_TEMP\_Aslains_Installer_Options.inf') then begin
        Logger.Text:=Logger.Text + 'Created: _Aslains_Installer_Options.inf' + #10#13;
        CopyFile(PChar(infparam),PChar(WorkingDir+'Loadinf_TEMP\_Aslains_Installer_Options.inf'),false );
      end;
    end
    else
      Logger.Text:=Logger.Text + 'File: _Aslains_Installer_Options.inf  - Not found!' + #10#13;

   end
   else
      Exit;      
end;

procedure TForm4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DeleteFile(WorkingDir+'Loadinf_TEMP\aslains_installer.exe');
  DeleteFile(WorkingDir+'Loadinf_TEMP\_Aslains_Installer_Options.inf');
  DeleteFile(WorkingDir+'Loadinf_TEMP\loadinf.bat');
  RemoveDir(WorkingDir+'Loadinf_TEMP');
  Timer1.Free;
end;

end.
