object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Form4'
  ClientHeight = 514
  ClientWidth = 644
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = InitializeForm
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 644
    Height = 401
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Logger: TRichEdit
      AlignWithMargins = True
      Left = 10
      Top = 30
      Width = 624
      Height = 361
      Margins.Left = 10
      Margins.Top = 30
      Margins.Right = 10
      Margins.Bottom = 10
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Font.Quality = fqAntialiased
      Lines.Strings = (
        'RichEdit1')
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
      Zoom = 100
    end
    object ThemeSellector: TComboBox
      Left = 67
      Top = 5
      Width = 140
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clDefault
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = 'Default'
      OnChange = ThemeChanger
      Items.Strings = (
        'Default'
        'Glossy'
        'Auric'
        'Light'
        'Emerald Light Slate')
    end
    object Theme_stxt: TStaticText
      Left = 26
      Top = 7
      Width = 40
      Height = 17
      Caption = 'Theme:'
      TabOrder = 2
    end
  end
  object BotPanel: TPanel
    Left = 0
    Top = 401
    Width = 644
    Height = 113
    Align = alBottom
    Anchors = [akBottom]
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      644
      113)
    object Panel3: TPanel
      Left = 441
      Top = 6
      Width = 153
      Height = 97
      Anchors = [akTop]
      BevelOuter = bvNone
      TabOrder = 2
      object BATButton: TButton
        AlignWithMargins = True
        Left = 3
        Top = 23
        Width = 147
        Height = 71
        Cursor = crHandPoint
        Align = alBottom
        Caption = 'Click to run the installer'
        Enabled = False
        TabOrder = 0
        OnClick = BATButtonClick
      end
      object inst_running: TStaticText
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 147
        Height = 17
        Align = alTop
        Alignment = taCenter
        Caption = 'NOT RUNNING'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Font.Quality = fqAntialiased
        ParentFont = False
        TabOrder = 1
        Transparent = False
      end
    end
    object Panel2: TPanel
      Left = 239
      Top = 6
      Width = 153
      Height = 97
      Anchors = [akTop]
      BevelOuter = bvNone
      TabOrder = 1
      object inf_load: TStaticText
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 147
        Height = 17
        Align = alTop
        Alignment = taCenter
        Caption = 'NOT LOADED'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Font.Quality = fqAntialiased
        ParentFont = False
        TabOrder = 0
        Transparent = False
      end
      object InfButton: TButton
        AlignWithMargins = True
        Left = 3
        Top = 23
        Width = 147
        Height = 71
        Cursor = crHandPoint
        Align = alBottom
        Caption = 'Click to load the .inf'
        Enabled = False
        TabOrder = 1
        OnClick = infLoaderClick
      end
    end
    object Panel1: TPanel
      Left = 38
      Top = 6
      Width = 153
      Height = 97
      BevelOuter = bvNone
      TabOrder = 0
      object exe_load: TStaticText
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 147
        Height = 17
        Align = alTop
        Alignment = taCenter
        Caption = 'NOT LOADED'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Font.Quality = fqAntialiased
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
      end
      object ExeButton: TButton
        AlignWithMargins = True
        Left = 3
        Top = 23
        Width = 147
        Height = 71
        Cursor = crHandPoint
        Align = alBottom
        Caption = 'Click to load the installer'
        TabOrder = 1
        OnClick = exeLoaderClick
      end
    end
  end
  object Timer1: TTimer
    Interval = 50
    OnTimer = OnTimer
    Left = 296
    Top = 144
  end
end
